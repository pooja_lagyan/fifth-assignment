package com.example.lagyan.studentsrecordusingdatabase.Activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.lagyan.studentsrecordusingdatabase.Comparators.*;
import com.example.lagyan.studentsrecordusingdatabase.Database.DBController;
import com.example.lagyan.studentsrecordusingdatabase.Entities.*;
import com.example.lagyan.studentsrecordusingdatabase.Navigation.DrawerItem;
import com.example.lagyan.studentsrecordusingdatabase.Navigation.CustomDrawerAdapter;
import com.example.lagyan.studentsrecordusingdatabase.util.*;
import com.example.lagyan.studentsrecordusingdatabase.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


public class WelcomePage extends AppCompatActivity implements View.OnClickListener, ReceiveDataInterface, Constants, MyDialogListener, CommonUtils.OnItemClickListener, AdapterView.OnItemClickListener {

    MyAdapter adapter;
    Intent intent;
    LinkedList<Student> studentsList;
    SharedPreferences sharedPreferences;
    int positionOfItem;
    Spinner spinner;
    RecyclerView recyclerView;
    Student studentObj;
    FragmentManager fragmentManager;
    AddStudentFragment addStudentFragment = new AddStudentFragment();
    RelativeLayout myLayout, fragmentLayout;
    Button listButton, gridButton;

    DrawerLayout drawerLayout;
    ListView drawerList;
    ActionBarDrawerToggle drawerToggle;
    CharSequence drawerTitle;
    CharSequence title;
    CustomDrawerAdapter customAdapter;
    List<DrawerItem> dataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_page);

        studentsList = new LinkedList<>();
        adapter = new MyAdapter(this, studentsList, R.layout.row_layout);
        sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);

        listButton = (Button) findViewById(R.id.listButton);
        gridButton = (Button) findViewById(R.id.gridButton);

        myLayout = (RelativeLayout) findViewById(R.id.myrelativelayout);
        fragmentLayout = (RelativeLayout) findViewById(R.id.fragmentlayout);
        spinner = (Spinner) findViewById(R.id.spinner);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        final ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.spinnerList));
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);

        /* Sorting according to item selected in spinner*/
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String sort = spinner.getSelectedItem().toString();
                if (sort.equals(SORT_ROLLNO_KEY)) {
                    Collections.sort(studentsList, new CompareByRollNO());
                    adapter.notifyDataSetChanged();
                } else if (sort.equals(SORT_NAME_KEY)) {
                    Collections.sort(studentsList, new CompareByName());
                    adapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        dataList = new ArrayList<>();
        title = drawerTitle = getTitle();
        drawerList = (ListView) findViewById(R.id.list_slidermenu);

        dataList.add(new DrawerItem(ADD_DETAILS, R.drawable.adduser));
        dataList.add(new DrawerItem(LOG_OUT, R.drawable.logout));
        customAdapter = new CustomDrawerAdapter(this, R.layout.custom_drawer_item, dataList);
        drawerList.setAdapter(customAdapter);
        drawerList.setOnItemClickListener(this);

        //This method makes the icon and title in the action bar clickable so that “up” (ancestral) navigation can be provided.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setHomeButtonEnabled(true);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(title);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(drawerTitle);
                invalidateOptionsMenu();
            }
        };
        drawerToggle.setDrawerIndicatorEnabled(true); //Enable or disable the drawer indicator.
        drawerLayout.setDrawerListener(drawerToggle);

        listButton.setOnClickListener(this);
        gridButton.setOnClickListener(this);
        adapter.setOnItemClickListener(this);
        new AsyncTasks().execute(VIEW_ALL_CODE);
    }

    /*Method to change the Recycler view to listview or gridview on Button Click*/
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.listButton:
                listButton.setTextColor(Color.rgb(0, 128, 255));
                gridButton.setTextColor(Color.rgb(160, 160, 160));
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                break;
            case R.id.gridButton:
                listButton.setTextColor(Color.rgb(160, 160, 160));
                gridButton.setTextColor(Color.rgb(0, 128, 255));
                recyclerView.setLayoutManager(new StaggeredGridLayoutManager(NUM_OF_COLMS, StaggeredGridLayoutManager.VERTICAL));
                break;
        }
    }

    /* Defining Method of ReceiveDataInterface to receive data of student from AddStudentFragment*/
    @Override
    public void recieveData(Student obj, int request) {
        unlockNavigationBar();
        getFragmentManager().beginTransaction().remove(addStudentFragment).commit();
        myLayout.setVisibility(View.VISIBLE);
        studentObj = obj;
        switch (request) {
            case ADD_DETAILS_CODE:
                new AsyncTasks().execute(ADD_STUDENT);
                studentsList.add(studentObj);
                Toast.makeText(this, studentObj.getStudentName() + ADD_DETAIL_KEY, Toast.LENGTH_SHORT).show();
                break;

            case EDIT_DETAILS_CODE:
                new AsyncTasks().execute(UPDATE_STUDENT);
                Toast.makeText(this, studentObj.getStudentName() + UPDATE_DETAIL_KEY, Toast.LENGTH_SHORT).show();
                break;

            case CLOSE:
                break;
        }
    }

    /*ListItem in Navigation Bar - Click listener*/
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case ADD:
                callFragment();
                lockNavigationBar();
                break;

            case LOGOUT:
                sharedPreferences.edit().clear().commit();
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    /*Writing data to database through Async task */
    class AsyncTasks extends AsyncTask<Integer, Student, Long> {
        private DBController dbControllerobj;
        private MyAdapter myAdapter;
        private int choice;
        ProgressDialog progressDialog;
        private long result;

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(WelcomePage.this);
            progressDialog.show();
            progressDialog.setMessage(DIALOG_MESSAGE);
            dbControllerobj = new DBController(WelcomePage.this);
            myAdapter = (MyAdapter) recyclerView.getAdapter();
        }


        @Override
        protected void onProgressUpdate(Student... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected Long doInBackground(Integer... params) {
            this.choice = params[0];
            switch (choice) {
                case VIEW_ALL_CODE:
                    studentsList = dbControllerobj.viewAll(studentsList);
                    break;

                case ADD_STUDENT:
                    result = dbControllerobj.insert(studentObj);
                    studentObj.rollNo = (int) result;
                    break;

                case DELETE_STUDENT:
                    result = dbControllerobj.delete(positionOfItem);
                    break;

                case UPDATE:
                    studentObj = dbControllerobj.view(positionOfItem);
                    break;

                case UPDATE_STUDENT:
                    result = dbControllerobj.update(studentObj);
                    break;

                case VIEW_SPECIFIC_CODE:
                    studentObj = dbControllerobj.view(positionOfItem);
                    break;
            }
            return result;
        }

        @Override
        protected void onPostExecute(Long aLong) {
            switch (choice) {
                case VIEW_SPECIFIC_CODE:
                    addStudentFragment.sendData(studentObj, VIEW_SPECIFIC_CODE);
                    break;
                case UPDATE:
                    addStudentFragment.sendData(studentObj, EDIT_DETAILS_CODE);
                    break;
            }
            if (result > 0) {
                studentsList = dbControllerobj.viewAll(studentsList);
            } else if (result == -1) {
                Toast.makeText(WelcomePage.this, UNSUCCESSFUL_TEXT, Toast.LENGTH_SHORT).show();
            }
            if (spinner.getSelectedItem().toString().equals(SORT_NAME_KEY)) {
                Collections.sort(studentsList, new CompareByName());
            }
            myAdapter.notifyDataSetChanged();
            progressDialog.dismiss();
        }

    }

    /* Defining functions for Item clicked in Dialog box */
    @Override
    public void onClick(int position) {
        switch (position) {
            case EDIT:
                callFragment();
                lockNavigationBar();
                new AsyncTasks().execute(UPDATE);
                break;

            case DISPLAY:
                callFragment();
                lockNavigationBar();
                new AsyncTasks().execute(VIEW_SPECIFIC_CODE);
                break;

            case DELETE:
                new AsyncTasks().execute(DELETE_STUDENT);
                new AsyncTasks().execute(VIEW_ALL_CODE);
                Toast.makeText(this, DATA_DELETED, Toast.LENGTH_SHORT).show();
                break;
        }
    }

    /* Dialog box On click Listener*/
    @Override
    public void onItemClick(int position) {
        Alert_Dialog dialogObj = new Alert_Dialog();
        dialogObj.setDialogListener(WelcomePage.this);
        positionOfItem = position;
        dialogObj.show(getFragmentManager(), DIALOG_KEY);
    }

    /* If we add fragment in this activity then this will lock the navigation bar and
    it will not be displayed while the fragment is opened */
    void lockNavigationBar() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        drawerLayout.closeDrawer(drawerList);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
    }

    /* If we close the fragment and come back to activity then it will unlock the navigation bar*/
    void unlockNavigationBar() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    /*Method to call the fragment */
    void callFragment() {
        myLayout.setVisibility(View.GONE);
        fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragmentlayout, addStudentFragment, "start");
        fragmentTransaction.commit();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return false;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void setTitle(CharSequence mtitle) {
        title = mtitle;
        getSupportActionBar().setTitle(title);
    }
}