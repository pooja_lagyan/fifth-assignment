package com.example.lagyan.studentsrecordusingdatabase.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lagyan.studentsrecordusingdatabase.R;
import com.example.lagyan.studentsrecordusingdatabase.util.Constants;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, Constants {

    TextView nametext, passwordtext;
    String name, password;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button login = (Button) findViewById(R.id.loginbutton);
        Button exit = (Button) findViewById(R.id.exitButton);
        nametext = (TextView) findViewById(R.id.name);
        passwordtext = (TextView) findViewById(R.id.password);
        sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);


        login.setOnClickListener(this);
        exit.setOnClickListener(this);

        /* It checks whether the user logged out last time or not.
        If no,then it automatically log in and loads the second activity"Welcome Page"*/
        if ((USERNAME.equals(sharedPreferences.getString(NAME_KEY, "")) && (PASSWORD.equals(sharedPreferences.getString(PASSWORD_KEY, null))))) {
            Intent intentObj = new Intent(this, WelcomePage.class);
            startActivity(intentObj);
            finish();
        }

        /*method To show Password when checkbox is checked*/
        CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox);
        final EditText password = (EditText) findViewById(R.id.password);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                } else {
                    password.setInputType(TYPE_TEXT_VARIATION_INVISIBLE);
                }
            }
        });
    }

    /*onClick listener of login and exit button*/
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginbutton:
                name = nametext.getText().toString();
                password = passwordtext.getText().toString();
                if (name.equals(USERNAME) && password.equals(PASSWORD)) {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(NAME_KEY, USERNAME);
                    editor.putString(PASSWORD_KEY, PASSWORD);
                    editor.commit();
                    Intent intentObj = new Intent(this, WelcomePage.class);
                    startActivity(intentObj);
                    finish();
                } else {
                    Toast.makeText(this, INCORRECT_CREDENTIALS, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.exitButton:
                ActivityCompat.finishAffinity(this);
                break;
        }
    }
}
