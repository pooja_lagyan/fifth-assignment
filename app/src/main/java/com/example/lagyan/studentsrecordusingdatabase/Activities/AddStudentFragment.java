package com.example.lagyan.studentsrecordusingdatabase.Activities;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lagyan.studentsrecordusingdatabase.Entities.Student;
import com.example.lagyan.studentsrecordusingdatabase.R;
import com.example.lagyan.studentsrecordusingdatabase.util.Constants;
import com.example.lagyan.studentsrecordusingdatabase.util.ReceiveDataInterface;
import com.example.lagyan.studentsrecordusingdatabase.util.SendDataInterface;

/**
 * Created by LAGYAN on 9/24/2015.
 */

public class AddStudentFragment extends Fragment implements Constants, View.OnClickListener, SendDataInterface {
    boolean isEditable = false;
    Button saveButton, cancelButton;
    TextView enterDataTextView;
    EditText nameText, addressText, mobileText;
    ReceiveDataInterface receiveDataObj;
    int rollno;
    InputMethodManager imm;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_add_student_data, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        receiveDataObj = (ReceiveDataInterface) getActivity();

        saveButton = (Button) getActivity().findViewById(R.id.saveButton);
        cancelButton = (Button) getActivity().findViewById(R.id.cancelButton);
        enterDataTextView = (TextView) getActivity().findViewById(R.id.enterdataText);
        nameText = (EditText) getActivity().findViewById(R.id.nameText);
        addressText = (EditText) getActivity().findViewById(R.id.addressText);
        mobileText = (EditText) getActivity().findViewById(R.id.mobileText);

        openKeyboard();
        enterDataTextView.setText(ENTER_DATA_TEXT);
        saveButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
    }

    /* OnClick listeners of Save and Cancel Button */
    @Override
    public void onClick(View v) {
        Student student = new Student();
        switch (v.getId()) {
            case R.id.saveButton:
                String name = nameText.getText().toString();
                String address = addressText.getText().toString();
                String mobile = mobileText.getText().toString();
                if (!name.isEmpty() && !address.isEmpty() && !mobile.isEmpty() && mobile.length() == 10) {
                    if (isEditable) {
                        student.setRollNo(rollno);
                        student.setStudentName(name);
                        student.setAddress(address);
                        student.setMobile(mobile);
                        clearEdittext();
                        closeKeyboard(v);
                        receiveDataObj.recieveData(student, EDIT_DETAILS_CODE);
                    } else {
                        student.setMobile(mobile);
                        student.setAddress(address);
                        student.setStudentName(name);
                        clearEdittext();
                        closeKeyboard(v);
                        receiveDataObj.recieveData(student, ADD_DETAILS_CODE);
                    }
                } else if (name.isEmpty() || address.isEmpty() || mobile.isEmpty()) {
                    Toast.makeText(getActivity(), ENTER_DATA_KEY, Toast.LENGTH_SHORT).show();
                } else if (mobile.length() < 10) {
                    Toast.makeText(getActivity(), INCORRECT_MOBILE_KEY, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.cancelButton:
                clearEdittext();
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                receiveDataObj.recieveData(null, CLOSE);
                break;
        }
    }

    /* Method to recieve the data of student from activity for editing and displaying it in this fragment's edittexts */
    @Override
    public void sendData(Student student, int request) {
        rollno = student.getRollNo();
        nameText.setText(student.getStudentName());
        mobileText.setText(student.getMobile());
        addressText.setText(student.getAddress());
        switch (request) {
            case VIEW_SPECIFIC_CODE:
                isEditable = false;
                nameText.setEnabled(isEditable);
                addressText.setEnabled(isEditable);
                mobileText.setEnabled(isEditable);
                enterDataTextView.setText(DISPLAY_DATA_TEXT + student.getRollNo());
                saveButton.setVisibility(View.INVISIBLE);
                nameText.setTextColor(Color.rgb(0, 0, 0));
                addressText.setTextColor(Color.rgb(0, 0, 0));
                mobileText.setTextColor(Color.rgb(0, 0, 0));
                break;

            case EDIT_DETAILS_CODE:
                isEditable = true;
                nameText.setEnabled(isEditable);
                addressText.setEnabled(isEditable);
                mobileText.setEnabled(isEditable);
                /*Move cursor to the end of text*/
                nameText.setSelection(nameText.getText().length());
                enterDataTextView.setText(EDIT_DATA_TEXT + student.getRollNo());
                saveButton.setVisibility(View.VISIBLE);
                openKeyboard();
                break;
        }
    }

    /*When the fragment is called again, the previously written data in Edittexts is not cleared,
       we need to clear it manually in case of fragments as fragments automatically reloads its previous state. */
    void clearEdittext() {
        nameText.setText("");
        addressText.setText("");
        mobileText.setText("");
    }

    // Show soft keyboard for the user to enter the value.
    void openKeyboard() {
        nameText.requestFocus();
        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(nameText, InputMethodManager.SHOW_FORCED);
    }

    //Close keyboard when activity is called
    void closeKeyboard(View v) {
        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
}
