package com.example.lagyan.studentsrecordusingdatabase.Entities;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lagyan.studentsrecordusingdatabase.Activities.WelcomePage;
import com.example.lagyan.studentsrecordusingdatabase.R;
import com.example.lagyan.studentsrecordusingdatabase.util.*;

import java.util.LinkedList;

/**
 * Created by LAGYAN on 9/4/2015.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.CustomViewHolder> {
    LinkedList<Student> students = new LinkedList<>();
    Context context;
    int rowLayout;
    CommonUtils.OnItemClickListener onItemClickListener;

    public MyAdapter(Context context, LinkedList<Student> students, int rowLayout) {
        this.context = context;
        this.students = students;
        this.rowLayout = rowLayout;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(rowLayout, null);
        CustomViewHolder customViewHolder = new CustomViewHolder(view);
        return customViewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, final int position) {
        holder.name.setText(students.get(position).getStudentName());
        holder.rollNo.setText(String.valueOf(students.get(position).getRollNo()));
        holder.address.setText(students.get(position).getAddress());
        holder.mobile.setText(students.get(position).getMobile());

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(students.get(position).getRollNo());
            }
        });
    }

    @Override
    public int getItemCount() {
        return students == null ? 0 : students.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        public TextView name, rollNo, address, mobile;
        public LinearLayout container;

        public CustomViewHolder(View itemView) {
            super(itemView);
            container = (LinearLayout) itemView.findViewById(R.id.container);
            name = (TextView) itemView.findViewById(R.id.name);
            rollNo = (TextView) itemView.findViewById(R.id.rollNo);
            address = (TextView) itemView.findViewById(R.id.address);
            mobile = (TextView) itemView.findViewById(R.id.mobile);
        }
    }

    public void setOnItemClickListener(CommonUtils.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
