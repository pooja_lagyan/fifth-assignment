package com.example.lagyan.studentsrecordusingdatabase.Database;

/**
 * Created by LAGYAN on 9/17/2015.
 */
public interface DBConstants {
    int VERSION = 4;
    String DATABASE_NAME = "StudentDB";
    String TABLE = "STUDENTS";
    String COLUMN_ROLLNO = "rollno";
    String COLUMN_NAME = "name";
    String COLUMN_ADDRESS = "address";
    String COLUMN_MOBILE = "mobile";
    String SELECT_QUERY = "SELECT * FROM " + TABLE;
    String DROP_QUERY = "DROP TABLE IF EXISTS " + TABLE;
    String CREATE_STUDENT_TABLE = "CREATE TABLE " + TABLE + " (" + COLUMN_ROLLNO + " INTEGER PRIMARY KEY , " +
            COLUMN_NAME + " TEXT ," + COLUMN_ADDRESS + " TEXT, " + COLUMN_MOBILE + " TEXT )";

}
