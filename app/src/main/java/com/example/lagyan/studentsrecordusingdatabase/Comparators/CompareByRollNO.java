package com.example.lagyan.studentsrecordusingdatabase.Comparators;

import com.example.lagyan.studentsrecordusingdatabase.Entities.*;

import java.util.Comparator;

/**
 * Created by LAGYAN on 9/9/2015.
 */
public class CompareByRollNO implements Comparator<Student> {

    @Override
    public int compare(Student lhs, Student rhs) {
        if (lhs.getRollNo() > rhs.getRollNo())
            return 1;
        else if (lhs.getRollNo() < rhs.getRollNo())
            return -1;
        else
            return 0;
    }
}
